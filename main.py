import time #import all of the needed libraries
import automationhat
import csv
import datetime
import signal
import plotly.plotly as py
import plotly.graph_objs as go
running = True
date = 0
sensorValue = 0
username = 'Povellesto'
api_key = 'cC5tbuz9AD1fENX3Nz0t'
stream_token = 'o3q8uff714'
py.sign_in(username, api_key)

trace1 = dict(
    x=[],
    y=[],
    hoverinfo='x+y',
    mode='lines',
    line=dict(width=0.5,
              color='rgb(111, 231, 219)'),
    stackgroup='one',
    stream=dict(
        token=stream_token,
        maxpoints=200,
    ),
)
layout = go.Layout(
    title='Moisture Sensor Data Stream',
    yaxis= go.layout.YAxis(
        title='Moisture Level',
        automargin=True,
        titlefont=dict(size=30),
    ),
)
fig = go.Figure(data=[trace1], layout=layout)
print(py.plot(fig, filename='Raspberry Pi Streaming TEST'))

def findDate():
    global date
    date = datetime.datetime.now().strftime("%H:%M:%S") #find date
    
def checkSensor(): #check the soil moister sensor, and check if the soil is dry. If so then turn on warn light.
    global sensorValue
    sensorValue = float(automationhat.analog.one.read())
    if sensorValue > 2:
            automationhat.light.warn.on()
    else:
        automationhat.light.warn.off()
        
def checkValve():
    if sensorValue >= 2:
        automationhat.relay.one.off()
    else:
        automationhat.relay.one.on()    
        
def main():
    automationhat.light.power.write(1)
    automationhat.light.comms.write(1)
    i = 0
    stream = py.Stream(stream_token)
    stream.open()
    
    while running == True:
        findDate()
        checkSensor()
        checkValve()
        sensor_data = float(automationhat.analog.one.read())
        stream.write({'x': i, 'y': sensor_data})
        i += 1
        # delay between stream posts
#	with open ('data.csv', 'w', newline='') as csvfile:
#		fieldnames = ['Time - 1/4 Sec', 'Moister Level - M3']
#		writer= csv.DictWriter(csvfile,fieldnames=fieldnames)
#		writer.writeheader()
#		write.writerow({'Time - 1/4  Sec':i, 'Moister Level - M3':sensor_data}) 

        time.sleep(0.25)
        
main()
        
