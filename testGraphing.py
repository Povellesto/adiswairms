import plotly.plotly as py
from plotly.graph_objs import Scatter, Layout, Figure
import time
import automationhat

username = 'Povellesto'
api_key = 'cC5tbuz9AD1fENX3Nz0t'
stream_token = 'o3q8uff714'

py.sign_in(username, api_key)

trace1 = Scatter(
    x=[],
    y=[],
    stream=dict(
        token=stream_token,
        maxpoints=200
    )
)

layout = Layout(
    title='Moisture Sensor Data Stream'
)

fig = Figure(data=[trace1], layout=layout)

print(py.plot(fig, filename='Raspberry Pi Streaming TEST'))


i = 0
stream = py.Stream(stream_token)
stream.open()

#the main sensor reading loop
while True:
        sensor_data = float(automationhat.analog.one.read())
        stream.write({'x': i, 'y': sensor_data})
        i += 1
        # delay between stream posts
        time.sleep(0.25)

